# Processo de associação

> **Para a sequência de tramas capturada no ficheiro disponibilizado indique:**

## 16)

> **Quais as duas ações realizadas (i.e., tramas enviadas) pelo host no trace
> imediatamente após t=49 para terminar a associação com o AP 30 Munroe St que
> estava ativa quando o trace teve início?  (Pista:  uma  é  na  camada  IP  e
> outra  na  camada  de  ligação  802.11).  Observando  a especificação 802.11,
> seria de esperar outra trama, mas que não aparece?**

![Terminação da associação com AP 30 Munroe
St](figures/16.png){#fig:deauthentication}

Para terminar a associação com AP 30 Munroe St é enviada uma trama _DHCP_ (_DHCP
Release_) e de seguida é enviada uma trama 802.11: _Deauthentication_.

## 17)

> **Examine o trace e procure tramas de authentication enviadas do host para um
> AP e vice-versa. Quantas mensagens de authentication foram enviadas do host
> para o AP linksys_ses_24086 (que tem o endereço MAC  Cisco_Li_f5:ba:bb)
> aproximadamente ao t=49?**

![Authentication requests](figures/17.png){#fig:authentication_requests}

Foram enviadas 6 mensagens _authentication_ do host para o AP
_linksys_ses_24086_, conforme se pode ver na @fig:authentication_requests a azul
claro.

## 18)

> **Qual o tipo de autenticação pretendida pelo host, aberta ou usando uma
> chave?**

![Autenticação](figures/18.png){#fig:18}

O tipo de autenticação pretendida pelo _host_ é aberta.

## 19)

> **Observa-se a resposta de authentication do AP linksys_ses_24086 AP no
> trace?**

Não se observa resposta por parte de _linksys_ses_24086_.

## 20)

> **Vamos  agora  considerar  o  que  acontece  quando  o host  desiste  de  se
> associar  ao  AP linksys_ses_24086 AP  e  se  tenta  associar  ao  AP 30
> Munroe  St.  Procure  tramas authenticationenviadas pelo host para e do AP e
> vice-versa. Em que tempo aparece um trama authentication do host para o AP 30
> Munroe St. e quando aparece a resposta authentication do AP para o host?**

![Autenticação com AP Munroe 30 St](figures/20.png){ #fig:20 }

Como podemos observar na @fig:20 o tempo em que aparece a trama _authentication_
para o AP 30 Munroe St é 63.168087. O _assciate reply_ é enviado no instante
63.169071.

## 21)

> **Um associate request do host para o AP e uma trama de associate response
> correspondente do AP para o host são usados para que o host seja associado a
> um AP. Quando aparece o associate request do host para o AP 30 Munroe St?
> Quando é enviado o correspondente associate reply?**

![Instante associate request](figures/associateRequest.PNG){#fig:ass_request}

![Instante associate response](figures/associateResponse.PNG){#fig:ass_response}

Podemos ver pelos prints que o associate request para o AP 30 Munroe St. aparece
no tempo 63.169910 e o associate response no tempo 63.192101.

## 22)

> **Que taxas de transmissão o host está disposto a usar? E o AP?**

![Data rate host ext](figures/dataRateHostExt.PNG){#fig:data_rate_ext_host}

![Data rate host sup](figures/dataRateHostSup.PNG){#fig:data_rate_host_sup}

![Data rate AP ext](figures/dataRateApExt.PNG){#fig:data_rate_ap_ext}

![Data rate AP sup](figures/dataRateApSup.PNG){#fig:data_rate_ap_sup}

Podemos ver as taxas de transmissão disponíveis no AP (30 Munroe St.) e a taxa
de transmissão do host nas tabelas.

## 23)

> **Identifique uma sequência de tramas que corresponda a um processo de
> associação completo entre a STA e o AP, incluindo a fase de autenticação.**

![Sequência de ligação](figures/seqLigacao.PNG){#fig:seq_ligacao}

![Filtro](figures/filtro23.PNG){#fig:filtro23}

É possível verificar que esta sequência de tramas corresponde a um processo de
associação compleo entre o STA e o AP (30 Munroe St.), pois existem as
autenticações e os respetivos associate request da STA e o associate response do
AP. Foi usado o filtro na imagem.

## 24)

> **Efetue um diagrama que ilustre a sequência de todas as tramas trocadas no
> processo de associação, incluindo a fase de autenticação.**

![Diagrama de Sequência de troca de tramas](figures/diagrama_autenticacao.png){ #fig:diagrama_autenticacao }
