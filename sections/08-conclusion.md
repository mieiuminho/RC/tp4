# Conclusão {#sec:conclusion}

A realização deste trabalho prático foi extremamente elucidativa, contribuindo
para um melhor entendimento das redes sem fios, tramas de gestão que permitem
que as estações (STAs) estabeleçam e mantenham comunicação. Consolidamos
conhecimento acerca das tramas de controlo que auxiliam a troca de dados entre
estações sem fios. Contactamos também com tramas de dados que permitem a
transmissão de dados de uma rede sem fios.

Conseguimos perceber ainda a importância das tramas _beacon_ no processo de
_scan_ passivo.

Tivemos contacto com o processo de autenticação e associação que deve acontecer
entre o _host_ e um ponto de acesso numa rede sem fios estruturada.

Por último conseguimos perceber a importância da interligação entre os tipos de
tramas descritas acima aquando da transferência de dados.
