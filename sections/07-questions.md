# Transferência de dados

## 25)

> **Encontre  a  trama  802.11  que  contém  o  segmento  SYN  TCP  para  a
> primeira  sessão  TCP (download alice.txt). Quais são os três campos dos
> endereços MAC na trama 802.11?**

Os 3 endereços MAC são os da tabela seguinte.

![Endereços MAC](figures/enderecosMAC_25.PNG){ #fig:MAC_25 }

## 26)

> **Qual o endereço MAC nesta trama que corresponde ao host (em notação
> hexadecimal)? Qual o do AP? Qual o do router do primeiro salto? Qual o
> endereço IP do host que está a enviar este segmento TCP? Qual o endereço IP de
> destino?**

Endereço do AP - Transmiter address
Endereço do 1 salto - Receiver Address
Endereço de destino - Destination Address

Endereços IP estão na imagem que se segue:

![Endereços de IP](figures/enderecIP_25.PNG){ #fig:IP_25 }

## 27)

> **Este  endereço  IP  de  destino  corresponde  ao host,  AP, router  do
> primeiro  salto,  ou  outro equipamento de rede? Justifique.**

O endereço de IP corresponde a outro equipamento de rede, uma vez que parte do
mesmo que identifica a rede é diferente.

## 28)

> **Encontre agora a trama 802.11 que contém o segmento SYNACK para esta sessão
> TCP. Quais são 6 os três campos dos endereços MAC na trama 802.11?**


![Trama contendo o segmento SYNACK](figures/SYNACK.png){ #fig:SYNACK }

A trama foi encontrada aplicando o filtro seguinte.

```
tcp.flags.syn==1 && tcp.flags.ack==1
```


## 29)

> **Qual o endereço MAC nesta trama que corresponde ao host? Qual o do AP? Qual
> o do router do primeiro salto?**

Endereço MAC do host: 00:16:b6:f4:eb:a8

Endereço do AP: 00:16:b6:f7:1d:51

Endereço do router do primeiro salto: 91:2a:b0:49:b6:4f

Assim como se pode ver na @fig:SYNACK.

## 30)

> **O endereço MAC de origem na trama corresponde ao endereço IP do dispositivo
> que enviou o segmento TCP encapsulado neste datagrama? Justifique.**

Não. O endereço MAC de origem é referente ao AP que enviou essa trama.

