# Scanning

> **As tramas beacon permitem efetuar scanning passivo em redes Wi-Fi. Para a
captura de tramas disponibilizada, responda às seguintes questões.**

## 4)

> **Quais são os SSIDs dos dois APs que estão a emitir a maioria das tramas
de beacon?**

![SSIDs do AP que emite a maioria das tramas beacon](figures/monroes.png){#fig:monroes}


Os SSIDs dos dois APs que estão a emitir a maioria das tramas de beacon são,
respetivamente: `30 Munroe St` e `linksys12`. Para obter contagem tramas
emitidas por cada AP utilizamos o seguinte comando: `wlan.ssid == "30 Munroe
St" && wlan.fc.type_subtype == 0x8`, sendo que para o segundo substituímos
`30 Munroe St` por `linksys12`.

## 5)

> **Qual o intervalo de tempo entre a transmissão de tramas beacon para o AP
linksys_ses_24086? E do AP 30 Munroe St? (Pista: o intervalo está contido na
própria trama). Na prática, a periodicidade de tramas beacon é verificada?
Tente explicar porquê. **

O intervalo de tempo esperado entre a trasmissão de tramas beacon para o AP
`30 Munroe St` é 0.102400.
O intervalo de tempo esperado entre a trasmissão de tramas beacon para o AP
`linksys_ses_24086` é 0.102400.

Na prática, a periodicidade não se verifica em ambos os casos visto que a
transmissão de tramas Beacon está condicionada ao tráfego da rede, razão pela
qual o pacote tem de esperar até ter prioridade na rede.

## 6)

> **Qual é (em notação hexadecimal) o endereço MAC de origem da trama beacon
de 30 Munroe St? Para detalhes sobre a estrutura das tramas 802.11, veja a
secção 7 da norma IEEE 802.11 citada no início. **

![Endereço MAC da origem da trama beacon `30 Munroe
St`](figures/source_destination.png){#fig:source_dest}

O endereço MAC de origem da trama beacon é `00:16:b6:f7:1d:51`, como se pode
confirmar na @fig:source_dest, a azul claro.

## 7)

> **Qual é (em notação hexadecimal) o endereço MAC de destino na trama de 30
Munroe St? **

O endereço MAC de destino da trama beacon é `ff:ff:ff:ff:ff:ff`, como se pode
confirmar na @fig:source_dest, a verde claro.

## 8)

> **Qual é (em notação hexadecimal) o MAC BSS ID da trama beacon de 30 Munroe
St?**

O endereço MAC BSS ID da trama beacon de 30 Munroe St é `00:16:b6:f7:1d:51`.

## 9)

> **As tramas beacon do AP 30 Munroe St anunciam que o AP suporta quatro data
rates e oito extended supported rates adicionais. Quais são?**

![Data Rates](figures/data_rates.png){#fig:data_rates}

![Extended Supported Rates](figures/extended_supported_rates.png){#fig:ext_sup_rates}

As quatro _data rates_ suportadas são aquelas que sugere @fig:data_rates. As
oito _extended supported rates_ são aquelas que sugere
@fig:ext_sup_rates.

## 10)

> **Selecione uma trama beacon (e.g., a trama 1YXX com Y=turno e XX=grupo,
e.g., 1101). Esta trama pertence a que tipo de tramas 802.11? Indique o valor
dos seus identificadores de tipo e de subtipo. Em que parte concreta do
cabeçalho da trama estão especificados (ver anexo)?**

![Tipo e sub-tipo da trama 802.11](figures/type_subtype.png){#fig:type_subtype}

Esta trama pertence ao tipo de tramas `Managment Frame` (tramas de gestão) e
ao subtipo: 8, conforme se verifica em @fig:type_subtype.

## 11)

> **Verifique se está a ser usado o método de deteção de erros CRC e se todas
as tramas beacon são recebidas corretamente. Justifique o uso de mecanismos
de deteção de erros neste tipo de redes locais.**

![FCS no fim da trama](figures/fcs_is_there.png){#fig:fcs_is_there}

![FCS](figures/fcs_not_used.png){#fig:fcs_not_used}

O método de deteção de erros CRC existe na trama, mas conforme é indicado pelo
campo `unverified` não está a ser verificado.

Uma vez que em redes wireless a probabilidade de ocorrer um erro na transmissão
da trama é maior, faz sentido que este método de deteção de erros seja usado
neste tipo de redes.

Nem todas as tramas beacon são transmitidas corretamente.

## 12)

> **Identifique e registe todos os endereços MAC usados nas tramas beacon
enviadas pelos APs. Recorde que o endereçamento está definido no cabeçalho
das tramas 802.11 podendo ser utilizados até quatro endereços com diferente
semântica. Para uma descrição detalhada da estrutura da trama 802.11,
consulte o anexo ao enunciado.**

![Endereços MAC usados](figures/used_mac_adresses.png){#fig:used_mac_addresses}

O primeiro MAC address é `ee:58:00:00:10:02`, o segundo é `85:09:a0:00:e1:9c`, o
terceiro é `64:00:00:45:bf:f6` e o quarto é `80:00:00:00:ff:ff`.

## 13)

> **Estabeleça um filtro Wireshark apropriado que lhe permita visualizar
todas as tramas probing request e probing response, simultaneamente.**

O filtro utilizado foi: `wlan.fc.type==0&&(wlan.fc.subtype==4 ||
wlan.fc.subtype==5)`.

## 14)

> **Quais são os endereços MAC BSS ID de destino e origem nestas tramas? Qual
o objetivo deste tipo de tramas?**

![Probe Request 1](figures/probe_request_1.png){#fig:prob_request1}

![Probe Request 2](figures/probe_request_2.png){#fig:prob_request2}

![Probe Response 1](figures/probe_response_1.png){#fig:prob_response1}

![Probe Response 2](figures/probe_response_2.png){#fig:prob_response2}

O objetivo deste tipo tramas é descobrir quais os APs que estão no seu alcance
rádio. Depois de enviado o _Probe Request_, sabe-se que os APs que responderem
estão no seu alcance rádio.

## 15)

> **Identifique  um probing  request  para  o  qual  tenha  havido  um probing
> response.  Face  ao endereçamento usado, indique a que sistemas são
> endereçadas estas tramas e explique qual o propósito das mesmas?**

![Probe request correspondido](figures/15.png){#fig:pr_replied}

Os tramas são endereaças a todas as interfaces ativas da sub-rede (broadcast).

O propósito destas tramas é descobrir quais os APs que estão no seu alcance
rádio.
