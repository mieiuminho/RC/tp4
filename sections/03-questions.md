# Acesso rádio

> **Para a trama correspondente com o número 1YXX (com Y=turno e XX=grupo,
e.g., 1101),**

## 1)

> **Identifique em que frequência do espectro está a operar a rede sem fios,
e o canal que corresponde a essa frequência.**

![Frequência de operação e canal
correspondente](figures/frequency_channel.png){#fig:freq_chan}

A frequência do espetro em que está a operar a rede sem fios é 2437 MHz, o
que corresponde ao canal 6, como se pode verifica na @fig:freq_chan, a azul
claro e verde respetivamente.

## 2)

> **Identifique a versão da norma IEEE 802.11 que está a ser usada.**

![Norma IEEE 802.11](figures/norm.png){#fig:norm}

A versão da norma IEEE 802.11 que está a ser usada é 0, como se pode verficar
na @fig:norm a azul claro.

## 3)

> **Qual o débito a que foi enviada a trama escolhida? Será que esse débito
corresponde ao débito máximo a que a interface Wi-Fi pode operar? Justifique.
**

![Débito a que a trama foi envidada](figures/data_rate.png){#fig:rate}

O débito que a trama escolhida foi enviada foi 24,0 Mbps, conforme podemos
ver na @fig:rate a azul claro. Este débito não corresponde ao débito máximo a
que interface Wi-Fi pode operar na norma IEEE 802.11, uma vez que esse limite
é de 54,0 Mbps, como se pode verificar em @ieee8211g.
